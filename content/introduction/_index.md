+++
title = "Introduction"
weight = 1
sort_by = "weight"
insert_anchor_links = "right"
+++

Practic - what is it and why do you need it.
<!-- more -->

In this introduction you will find out ideas behind the Practic and goals of it.
Basic concepts are explained here and resources for further investigations are provided.
