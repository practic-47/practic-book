+++
title = "Basic Concepts"
weight = 2
+++

Some basic concepts should be defined here to agree on the terms we use in Practic
and to share some context with you.

## Domain

Practic's main field is education. It may be an academia, consulting, trainings or couching.
To cover all these fields we will use "knowledge sharing" as a primary domain. Several concepts
defined below will help us to speak on the same 'language'.

### School

Let's get abstract over details - if you give knowledge you are a [School](@/appendix_d/catalog.md#school)
for your [students](@/appendix_d/catalog.md#student). They are here to know something new.
`School` has a title, stuff and other properties (see [Appendix D](@/appendix_d/catalog.md#school) to find
all of them).  The most interesting part is an educational content.

### Course

And if `School` is a place where we go, [Course](@/appendix_d/catalog.md#course) is a goal why we go.
`Course` is a knowledge that you share with your `students`. Practic gives a lot of tools to work with 
this knowledge and we will [create our first course](@/creating_course/_index.md) very soon.

### People

Before we talked about things and now we will find out who and how can work with them. On one side
we have [academics](@/appendix_d/catalog.md#academic) and their main goal is to share their knowledge.
On the other side we have [students](@/appendix_d/catalog.md#student) and they want to get it.

## Processes

And if we had have only these things in Practic, there are a lot of such a tools [around there](@/appendix_c/_index.md).
What we really want to do and where we want to help you is processes. How to make it easier to work 
with sales? Can we automate risks management? Is it a better way to build communications?
All these questions were in our minds when we started Practic's development, so we have answers and
we are really happy to share them with you.

//TODO: Describe basic concepts in sales and communications.
