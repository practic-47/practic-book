+++
title = "Resources"
weight = 3
+++

Where to find more information that you are interested in?

If you are a potential customer - look at our [home page](https://practic-47.gitlab.io/practic-landing/)
and feel free to [contact us](https://practic-47.gitlab.io/practic-landing/#contact).

If you are a user and interested in exploring all Practic's features in depth,
keep moving through our book and feel free
to jump over some section (don't forget to return later and find more cool things).

If you you want to add a new feature and make Practic a little bit (or a lot) better,
go straight to our [contributing guide](@/contribution/_index.md).
