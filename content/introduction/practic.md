+++
title = "Practic"
weight = 1
+++

Practic is a way to make teachers, trainers and educational businesses life free from daily routine,
allowing them to focus their skills and activities on the important things.

## Our Values

Practic follows these values and share our philosophy with customers, users, contributors and partners:

- Open and free - working honestly and making our processes visible we build trust.
- Passion - all what we do we want to do and developing the product we love we know that people will love it too.
- Design - trying to be smart and agile and creating solutions' design and architecture clear we build our future.
- Support - finding an honest and professional partners and providing simple ways to find a help we make our customers and users happy. 
