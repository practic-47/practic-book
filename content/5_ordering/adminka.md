+++
title = "Working with 'Adminka'"
weight = 2
[taxonomies]
tags = ["catalog", "adminka"]
categories = ["tutorial"]
+++

`Adminka` is a tool to work with catalog entities in terms of management.
`Students` and `Potential customers` can only consume knowledge from the course.
`Academics` and `Administrators` are able to create new courses, update materials, etc.

## Hands on 

Let's find out what we can do in `Adminka`. If you already [created your first course](@/creating_course/_index.md),
you can go to [Making Mistakes](#making-mistakes) step of this tutorial.
If not, login into Practic as `Academic` or `Admin`.
`Adminka` will open a `Dashboard` tab first, you can look at it's information, but we need to change our tab.
Click `General` tab - you will see a list of already created courses (or empty table if there is no courses yet).
To create a new course you should fill in information about that course and click "Add Course" button.
Table will be updated with information about newly created course, you are doing great!

## Making Mistakes

//TODO: write other sections.
