+++
title = "How to buy a course"
weight = 1
[taxonomies]
tags = ["ordering", "buy", "course"]
categories = ["how-to"]
+++

In terms of business, if user didn't bought a course yet, he or she is a `potential student`.
To become a `student` user needs to buy a course. How to do that? Let's find out in this guide.

//TODO: describe steps.
